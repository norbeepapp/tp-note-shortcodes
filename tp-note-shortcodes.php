<?php
/*
Plugin Name: TP Note shortcodes
Plugin URI: https://tarhelypark.hu
Description: A Tárhelypark.hu-n és Számlahegy.hu-n használt Note shortcode-ok pluginba kovácsolva. A Font-Awesome a Bootstrap CDN-ről származik.
Version: 0.1
Author: Norbert Papp
Author URI: https://tarhelypark.hu
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// Enqueues external Font Awesome stylesheet
function enqueue_our_required_stylesheets(){
	wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
}
add_action('wp_enqueue_scripts','enqueue_our_required_stylesheets');

// Register TP Note styles
function tp_note_styles() {
	wp_register_style( 'tp_note_stylesheet', plugins_url('style.css',__FILE__ ));
	wp_enqueue_style( 'tp_note_stylesheet' );
}
add_action( 'wp_enqueue_scripts', 'tp_note_styles' );

function tp_pricing_toggle() {
	wp_register_script( 'tp_pricing_scripts', plugins_url('pricing-toggle.js',__FILE__ ), false, '1.0',true);
	wp_enqueue_script( 'tp_pricing_scripts' );
}
add_action( 'wp_enqueue_scripts', 'tp_pricing_toggle' );

// TP shortcodes [tpnote title="Custom title"]
function tp_note_shortcode( $atts, $content = null, $tag ) {
    $atts = shortcode_atts(
    	array(
			'title' => 'Jó tudni!',
		),
		$atts
	);
    switch( $tag ) {
        case "note":
            $src = '<div class="note-box tpnote"><div class="note-icon"><span><i class="fa fa-info" aria-hidden="true"></i> </span></div> <div class="note-text"><h3>'. $atts[ 'title' ] . '</h3><p>' . $content . '</p></div></div>';
            break;
        case "alert":
            $src = '<div class="note-box alert"><div class="note-icon"><span><i class="fa fa-exclamation" aria-hidden="true"></i> </span></div> <div class="note-text"><h3>'. $atts[ 'title' ] . '</h3><p>' . $content . '</p></div></div>';
            break;
        case "idea":
            $src = '<div class="note-box idea"><div class="note-icon"><span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> </span></div> <div class="note-text"><h3>'. $atts[ 'title' ] . '</h3><p>' . $content . '</p></div></div>';
            break;
        case "success":
            $src = '<div class="note-box success"><div class="note-icon"><span><i class="fa fa-check" aria-hidden="true"></i> </span></div> <div class="note-text"><h3>'. $atts[ 'title' ] . '</h3><p>' . $content . '</p></div></div>';
            break;
    }

  return $src;
}
add_shortcode( 'note', 'tp_note_shortcode' );
add_shortcode( 'alert', 'tp_note_shortcode' );
add_shortcode( 'idea', 'tp_note_shortcode' );
add_shortcode( 'success', 'tp_note_shortcode' );

// Add Pricing table Shortcode [pricing-table monthly_text="Havi" yearly_text="Éves"][/pricing-table]
function pricing_table( $atts , $content = null ) {
	$atts = shortcode_atts(
		array(
			'monthly_text' => 'Havi',
			'yearly_text' => 'Éves',
		),
		$atts
	);
	$src = '<div class="pricing-container"> <div class="pricing-switcher"> <div class="simple-toggler"> <label class="toggler toggler--is-active" id="filt-monthly">'. $atts[ 'monthly_text' ] . '</label> <div class="toggle"> <input type="checkbox" id="switcher" class="check"> <b class="b switcher"></b> </div> <label class="toggler" id="filt-yearly">'. $atts[ 'yearly_text' ] . '</label> </div> </div>' . do_shortcode($content) . '</div>';
	return $src;
}

add_shortcode( 'pricing-table', 'pricing_table' );

// Add Pricing list Shortcode [pricing-list billing="monthly"] [/pricing-list]
function pricing_list( $atts, $content = null, $tag ) {
    $atts = shortcode_atts(
    	array(
			'billing' => 'monthly',
		),
		$atts
	);
    switch( $tag ) {
        case "pricing-list-monthly":
        	$src = '<ul id="monthly" class="pricing-list">' . do_shortcode($content) . '</ul>';
        break;
        case "pricing-list-yearly":
          $src = '<ul id="yearly" class="pricing-list hide">' . do_shortcode($content) . '</ul>';
        break;
    }

	return $src;
}

add_shortcode( 'pricing-list-monthly', 'pricing_list' );
add_shortcode( 'pricing-list-yearly', 'pricing_list' );

// Add Pricing list Shortcode [pricing-item name="" price="" billing="monthly" button_text="" button_link=""] [/pricing-item]
function pricing_item( $atts, $content = null, $tag ) {
    $atts = shortcode_atts(
    	array(
			'featured' => '0',
			'name' => 'Csomag',
			'price' => '9990',
			'currency' => 'Ft',
			'billing_text' => 'havonta',
			'button_text' => 'Regisztrálok',
			'button_link' => '#',
		),
		$atts
	);
	// '. $atts[ 'billing' ] . ' ' . do_shortcode($content) . '
  $featuredClass = null;
	if ($atts['featured'] == 1) {
  	$featuredClass = ' class="pricing-featured" ';
	}
	$src = '<li '. $featuredClass . '> <ul class="pricing-wrapper"> <li> <header class="pricing-header"> <h2>'. $atts[ 'name' ] . '</h2> <div class="price"> <span class="value">'. $atts[ 'price' ] . '</span> <span class="currency">'. $atts[ 'currency' ] . '</span><br /> <span class="duration">'. $atts[ 'billing_text' ] . '</span> </div> </header> <div class="pricing-body"> <ul class="pricing-features">' . $content . '</ul> </div> <footer class="pricing-footer"> <a class="pricing-button" href="'. $atts[ 'button_link' ] . '" target="_blank" rel="nofollow">'. $atts[ 'button_text' ] . '</a> </footer> </li> </ul> </li>';
	return $src;
}

add_shortcode( 'pricing-item', 'pricing_item' );
