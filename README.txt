=== TP Note Shortcodes ===
Contributors: partisan1991
Tags: note, shortcodes, styles, alert, idea, success,
Requires at least: 4.6
Tested up to: 4.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Simple note styles for your blog, using Font Awesome icons.

== Description ==
-