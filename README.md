# TP Note Shortcodes #

Note styles plugin for Wordpress. Based on my static styles for Tárhelypark: http://codepen.io/partisan1991/full/gMLRdB/

### Features ###

* Font Awesome icons
* No messing with typography, only display and color sets

### How do I get set up? ###

* Download as .zip
* Install and activate via Wordpress Plugins